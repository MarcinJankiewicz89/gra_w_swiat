package swiat;

import swiat.organizmy.Organizm;
import swiat.organizmy.rosliny.Guarana;
import swiat.organizmy.rosliny.Mlecz;
import swiat.organizmy.rosliny.Trawa;
import swiat.organizmy.rosliny.WilczeJagody;
import swiat.organizmy.zwierzeta.*;

import java.util.*;

public class Swiat {

    private  Organizm[][] organizmy;
    private  int rozmiarSwiatX;
    private  int rozmiarSwiatY;
    private  int wspYorganizmu;
    private  int wspXorganizmu;
    private Random ran = new Random();
    private int losowaLiczba;
    private int wielkoscSwiata;
    private int iloscOrganizmowWSwiecie = 9;
    private int[] iloscOrganizmow = new int[iloscOrganizmowWSwiecie + 1];
    public List<Organizm> listaOrganizmow = new LinkedList<>();


    public Swiat() {
    }

    public void wykonajTure() {
        postarzejWszystkich();
        ustalKolejnoscRuchu();
        for(Organizm organizm: listaOrganizmow){
            if(organizm.isCzyZyje()){
            wspYorganizmu = organizm.getPolozenie().getWspY();
            wspXorganizmu = organizm.getPolozenie().getWspX();
            organizm.akcja();}
        }
        listaOrganizmow.clear();

    }

    private void ustalKolejnoscRuchu() {
        for (int i = 0; i < organizmy.length; i++) {
            for (int j = 0; j < organizmy[i].length; j++) {
                if (organizmy[i][j] != null) {
                    listaOrganizmow.add(organizmy[i][j]);
                }
            }
        }
        //Collections.sort(listaOrganizmow,Comparator.comparing(Organizm::getInicjatywa).thenComparing(Organizm::getWiek).reversed());
        listaOrganizmow.sort(Comparator.comparing(Organizm::getInicjatywa).thenComparing(Organizm::getWiek).reversed());
    }

    private void postarzejWszystkich() {
        for (int i = 0; i < organizmy.length; i++) {
            for (int j = 0; j < organizmy[i].length; j++) {
                if (organizmy[i][j] != null)
                    organizmy[i][j].setWiek(organizmy[i][j].getWiek() + 1);
            }
        }
    }


    public void rozpocznijRozgrywke() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj w nowym świecie :)");
        System.out.println("Podaj wymiary Twojego świata. Y x X :");
        System.out.print("Y=");
        rozmiarSwiatY = scanner.nextInt();
        System.out.print("X=");
        rozmiarSwiatX = scanner.nextInt();
        wielkoscSwiata = rozmiarSwiatX * rozmiarSwiatY;
        organizmy = new Organizm[rozmiarSwiatY][rozmiarSwiatX]; //Określenie wielkości świata. Y- wiersze, X - kolumny
        losujIloscOrganizmow();
        zapelnijSwiat();
    }

    public void zapelnijSwiat() {
        int wspY, wspX;
        for (int i = 0; i < iloscOrganizmow.length; i++) {
            int j = 0;
            while (j < iloscOrganizmow[i]) {
                wspY = losujWspY();
                wspX = losujWspX();
                switch (i) {
                    case 0:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Guarana(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 1:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Mlecz(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 2:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Trawa(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 3:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new WilczeJagody(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 4:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Antylopa(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 5:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Lis(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 6:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Owca(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 7:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Wilk(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 8:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Zolw(this, wspY, wspX);
                            j++;
                        }
                        break;
                    case 9:
                        if (organizmy[wspY][wspX] == null) {
                            organizmy[wspY][wspX] = new Czlowiek(this, wspY, wspX);
                            j++;
                        }
                        break;
                }
            }
        }
    }

    public void rysujSwiat() {
        for (int i = 0; i < organizmy.length; i++) {
            for (int j = 0; j < organizmy[i].length; j++) {
                if (organizmy[i][j] == null) {
                    System.out.print(".\t");
                } else {
                    organizmy[i][j].rysowanie();
                }
            }
            System.out.println();
        }
    }


    // Tą klasę należy porzeszyć za każdym razem, gdy dojdzie nowa klasa Rośliny lub Zwierząt
    public void dajMlodyOrganizm(int wspY, int wspX) {
        // Nowe instancje Zwierząt:
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Wilk)
            organizmy[wspY][wspX] = new Wilk(this, wspY, wspX);
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Owca)
            organizmy[wspY][wspX] = new Owca(this, wspY, wspX);
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Lis)
            organizmy[wspY][wspX] = new Lis(this, wspY, wspX);
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Zolw)
            organizmy[wspY][wspX] = new Zolw(this, wspY, wspX);
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Antylopa)
            organizmy[wspY][wspX] = new Antylopa(this, wspY, wspX);

        // Tu będą nowe instancje Roślin:
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Trawa)
            organizmy[wspY][wspX] = new Trawa(this, wspY, wspX);
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Mlecz)
            organizmy[wspY][wspX] = new Mlecz(this, wspY, wspX);
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof Guarana)
            organizmy[wspY][wspX] = new Guarana(this, wspY, wspX);
        if (organizmy[wspYorganizmu][wspXorganizmu] instanceof WilczeJagody)
            organizmy[wspY][wspX] = new WilczeJagody(this, wspY, wspX);
    }


    /*Losowanie ilości poszczególnych zwierząt, które pojawią się w świecie.
    wg wzoru: ilośćOrganizmówDanegoRodzaju = ran.nextInt( wielkośćSwiata / ilośćOrganizmowWSwiecie ) + 1;
    losowanie po kolei:
    0 - Gurana
    1 - Mlecz
    2 - Trawa
    3 - WilczeJagody
    4 - Antylopa
    5 - Lis
    6 - Owca
    7 - Wilk
    8 - Zolw
    9 - Czlowiek -> ilosc = 1
    */
    private void losujIloscOrganizmow() {
        for (int i = 0; i < iloscOrganizmowWSwiecie; i++) {
            iloscOrganizmow[i] = ran.nextInt(wielkoscSwiata / iloscOrganizmowWSwiecie) + 1;
        }
        iloscOrganizmow[iloscOrganizmowWSwiecie] = 1;
    }

    private int losujWspY() {
        return ran.nextInt(rozmiarSwiatY);
    }

    private int losujWspX() {
        return ran.nextInt(rozmiarSwiatX);
    }


    public Organizm zwrocOrganizm() {
        return organizmy[wspYorganizmu][wspXorganizmu];
    }

    public void setOrganizm(Organizm organizm, int wspY, int wspX) {
        organizmy[wspY][wspX] = organizm;
    }

    public void setOrganizm(Organizm organizm) {
        organizmy[wspYorganizmu][wspXorganizmu] = organizm;
    }

    public int getWspYorganizmu() {
        return wspYorganizmu;
    }

    public void setWspYorganizmu(int wspYorganizmu) {
        this.wspYorganizmu = wspYorganizmu;
    }

    public int getWspXorganizmu() {
        return wspXorganizmu;
    }

    public void setWspXorganizmu(int wspXorganizmu) {
        this.wspXorganizmu = wspXorganizmu;
    }

    public Organizm[][] getOrganizmy() {
        return organizmy;
    }

    public Organizm getOrganizm(int wspY, int wspX) {
        return organizmy[wspY][wspX];
    }

    public int getRozmiarSwiatX() {
        return rozmiarSwiatX;
    }

    public int getRozmiarSwiatY() {
        return rozmiarSwiatY;
    }

}
