package swiat.polozenie;

public class Polozenie {
    private int wspX;
    private int wspY;

    public Polozenie() {
    }

    public Polozenie(int wspY, int wspX) {
        this.wspX = wspX;
        this.wspY = wspY;
    }

    public boolean porownajPolozenia(int y, int x) {
        if (y == wspY && x == wspX)
            return true;
        else return false;
    }

    public void setWspX(int wspX) {
        this.wspX = wspX;
    }

    public void setWspY(int wspY) {
        this.wspY = wspY;
    }

    public int getWspX() {
        return wspX;
    }

    public int getWspY() {
        return wspY;
    }


}
