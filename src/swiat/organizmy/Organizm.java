package swiat.organizmy;

import swiat.Swiat;
import swiat.polozenie.Polozenie;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Organizm {
    protected int sila;
    protected int inicjatywa;
    protected int wiek;
    protected char obraz;
    protected Polozenie polozenie;
    protected boolean czyZyje;
    protected Swiat swiat; // nie używane...
    protected int krok;
    protected int wspXSasiada, wspYSasiada;
    protected int wspXPolePuste, wspYPolePuste;
    protected boolean ruchLewo, ruchPrawo, ruchGora, ruchDol;
    protected int kierunekRuchu;
    private static List<Integer> wspDziecka;

    public void setSila(int sila) {
        this.sila = sila;
    }


    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public void setKierunekRuchu(int kierunekRuchu) {
        this.kierunekRuchu = kierunekRuchu;
    }

    public void akcja() {
    }

    // KOLIZJA.
    // Jeśli organizm atakujący ma siłę równą lub większą to zwycięża, jeśli mniejszą przegrywa.
    public void kolizja() {

    }

    //Rysowanie obrazu organizmu
    public void rysowanie() {
        System.out.print(obraz + "\t");
    }

    // sprawdzenie, które kierunki ruchu są możliwe do podjęcia
    // np. jeśli organizm jest na górnej krawędzi świata (wspY=0) to nie moze się ruszyć do góry
    protected void okreslMozliwosciRuchu() {
        //Sprawdzenie czy można się ruszyć do góry
        if (this.polozenie.getWspY() == 0)
            this.ruchGora = false;
        else
            this.ruchGora = true;
        // czy można się ruszyć do dołu
        if (this.polozenie.getWspY() == swiat.getRozmiarSwiatY() - 1)
            this.ruchDol = false;
        else
            this.ruchDol = true;
        //czy można się ruszyć w lewo
        if (this.polozenie.getWspX() == 0)
            this.ruchLewo = false;
        else
            this.ruchLewo = true;
        //czy można się ryszyć w prawo
        if (this.polozenie.getWspX() == swiat.getRozmiarSwiatX() - 1)
            this.ruchPrawo = false;
        else
            this.ruchPrawo = true;
    }

    // wylosowanie kierunku ruchu dla organizmu z uwzględnieniem możliwości ruchu
    protected void losujKierunekRuchu() {
        okreslMozliwosciRuchu();
        Random ran = new Random();
        boolean czyWybranoKierunek = false;
        /*
        Kierunki:
        1 - ruch w Lewo,
        2 - ruch w Prawo,
        3 - ruch w Górę,
        4 - ruch w Dół.
         */
        do {
            kierunekRuchu = ran.nextInt(4) + 1;
            if (kierunekRuchu == 1 && ruchLewo)
                czyWybranoKierunek = true;
            if (kierunekRuchu == 2 && ruchPrawo)
                czyWybranoKierunek = true;
            if (kierunekRuchu == 3 && ruchGora)
                czyWybranoKierunek = true;
            if (kierunekRuchu == 4 && ruchDol)
                czyWybranoKierunek = true;
        } while (!czyWybranoKierunek);
    }

    protected void ruszajWLewo() {
        this.polozenie.setWspX(this.polozenie.getWspX() - krok);
    }

    protected void ruszajWPrawo() {
        this.polozenie.setWspX(this.polozenie.getWspX() + krok);
    }

    protected void ruszajWGore() {
        this.polozenie.setWspY(this.polozenie.getWspY() - krok);
    }

    protected void ruszajWDol() {
        this.polozenie.setWspY(this.polozenie.getWspY() + krok);
    }

    // Ruch organizmu - właściwy tylko dla zwierząt.
    protected void ruszSie() {
        Organizm chwiloweZycie = this;
        System.out.print(obraz + " (" + polozenie.getWspY() + "," + polozenie.getWspX() + ") ruch: ");
        swiat.setOrganizm(null);
        switch (kierunekRuchu) {
            case 1:
                ruszajWLewo();
                System.out.println("Lewo");
                break;
            case 2:
                ruszajWPrawo();
                System.out.println("Prawo");
                break;
            case 3:
                ruszajWGore();
                System.out.println("Góra");
                break;
            case 4:
                ruszajWDol();
                System.out.println("Dół");
                break;
        }
        swiat.setOrganizm(chwiloweZycie, this.polozenie.getWspY(), this.polozenie.getWspX());
    }

    // sprawdzenie czy sąsiednie pole, na które chce się ruszyć/ rozprzestrzenić jest puste
    protected boolean sprawdzCzyPolePuste() {
        switch (kierunekRuchu) {
            case 1: //co po lewej stronie?
                if (polozenie.getWspX() - krok < 0)
                    krok--;
                if (swiat.getOrganizmy()[polozenie.getWspY()][polozenie.getWspX() - krok] == null)
                    return true;
                break;

            case 2: // co po prawej?
                if (polozenie.getWspX() + krok > swiat.getRozmiarSwiatX() - 1)
                    krok--;
                if (swiat.getOrganizmy()[polozenie.getWspY()][polozenie.getWspX() + krok] == null)
                    return true;
                break;

            case 3: // co na górze?
                if (polozenie.getWspY() - krok < 0)
                    krok--;
                if (swiat.getOrganizmy()[polozenie.getWspY() - krok][polozenie.getWspX()] == null)
                    return true;
                break;

            case 4: // no na dole?
                if (polozenie.getWspY() + krok > swiat.getRozmiarSwiatY() - 1)
                    krok--;
                if (swiat.getOrganizmy()[polozenie.getWspY() + krok][polozenie.getWspX()] == null)
                    return true;
                break;
        }
        return false;
    }


    protected void ustawWSP_polaPustego() {
        switch (kierunekRuchu) {
            case 1: //ustaw sasiada po lewej?
                wspYPolePuste = polozenie.getWspY();
                wspXPolePuste = polozenie.getWspX() - krok;
                break;
            case 2: //ustaw sasiada po prawej?
                wspYPolePuste = polozenie.getWspY();
                wspXPolePuste = polozenie.getWspX() + krok;
                break;
            case 3: //ustaw sasiada na górze?
                wspYPolePuste = polozenie.getWspY() - krok;
                wspXPolePuste = polozenie.getWspX();
                break;
            case 4: //ustaw sasiada na dole?
                wspYPolePuste = polozenie.getWspY() + krok;
                wspXPolePuste = polozenie.getWspX();
                break;
        }
    }


    // Ustawia współrzędne pola sąsiada
    protected void ustawWSP_polaSasiada() {
        switch (kierunekRuchu) {
            case 1: //ustaw sasiada po lewej?
                wspYSasiada = polozenie.getWspY();
                wspXSasiada = polozenie.getWspX() - krok;
                break;
            case 2: //ustaw sasiada po prawej?
                wspYSasiada = polozenie.getWspY();
                wspXSasiada = polozenie.getWspX() + krok;
                break;
            case 3: //ustaw sasiada na górze?
                wspYSasiada = polozenie.getWspY() - krok;
                wspXSasiada = polozenie.getWspX();
                break;
            case 4: //ustaw sasiada na dole?
                wspYSasiada = polozenie.getWspY() + krok;
                wspXSasiada = polozenie.getWspX();
                break;
        }
    }

    // znajdowanie pustych pól sąsiadujących z polami jednego z rodziców w przypadku rozmnażania zwierząt
    protected void znajdzPustePola(int wspYSasiada, int wspXSasiada) {
        wspDziecka = new ArrayList<>();
        int minY = Math.min(polozenie.getWspY(), wspYSasiada);
        int maxY = Math.max(polozenie.getWspY(), wspYSasiada);
        int minX = Math.min(polozenie.getWspX(), wspXSasiada);
        int maxX = Math.max(polozenie.getWspX(), wspXSasiada);
        if (minY != 0) {
            if (swiat.getOrganizmy()[minY - 1][minX] == null) {
                wspDziecka.add(minY - 1);
                wspDziecka.add(minX);
            }
            if (swiat.getOrganizmy()[minY - 1][maxX] == null) {
                wspDziecka.add(minY - 1);
                wspDziecka.add(maxX);
            }
        }
        if (maxY != swiat.getRozmiarSwiatY() - 1) {
            if (swiat.getOrganizmy()[maxY + 1][minX] == null) {
                wspDziecka.add(maxY + 1);
                wspDziecka.add(minX);
            }
            if (swiat.getOrganizmy()[maxY + 1][maxX] == null) {
                wspDziecka.add(maxY + 1);
                wspDziecka.add(maxX);
            }
        }
        if (minX != 0) {
            if (swiat.getOrganizmy()[minY][minX - 1] == null) {
                wspDziecka.add(minY);
                wspDziecka.add(minX - 1);
            }
            if (swiat.getOrganizmy()[maxY][minX - 1] == null) {
                wspDziecka.add(maxY);
                wspDziecka.add(minX - 1);
            }
        }
        if (maxX != swiat.getRozmiarSwiatX() - 1) {
            if (swiat.getOrganizmy()[minY][maxX + 1] == null) {
                wspDziecka.add(minY);
                wspDziecka.add(maxX + 1);
            }
            if (swiat.getOrganizmy()[maxY][maxX + 1] == null) {
                wspDziecka.add(maxY);
                wspDziecka.add(maxX + 1);
            }
        }
    }

    protected int losowaLiczba(int liczba) {
        Random ran = new Random();
        return ran.nextInt(liczba);
    }

    protected Polozenie zwrocPustePolozenie() {
        if (wspYSasiada != 0) {
            if (swiat.getOrganizmy()[wspYSasiada - 1][wspXSasiada] == null) {
                return new Polozenie(wspYSasiada - 1, wspXSasiada);
            }

        }
        if (wspYSasiada != swiat.getRozmiarSwiatY() - 1) {
            if (swiat.getOrganizmy()[wspYSasiada + 1][wspXSasiada] == null) {
                return new Polozenie(wspYSasiada + 1, wspXSasiada);

            }
        }
        if (wspXSasiada != 0) {
            if (swiat.getOrganizmy()[wspYSasiada][wspXSasiada - 1] == null) {
                return new Polozenie(wspYSasiada, wspXSasiada - 1);

            }
        }
        if (wspXSasiada != swiat.getRozmiarSwiatX() - 1) {
            if (swiat.getOrganizmy()[wspYSasiada][wspXSasiada + 1] == null) {
                return new Polozenie(wspYSasiada, wspXSasiada + 1);
            }
        }
        return this.polozenie;
    }


    public int getSila() {
        return sila;
    }

    public int getInicjatywa() {
        return inicjatywa;
    }

    public int getWiek() {
        return wiek;
    }

    public char getObraz() {
        return obraz;
    }

    public boolean isCzyZyje() {
        return czyZyje;
    }

    public void setCzyZyje(boolean czyZyje) {
        this.czyZyje = czyZyje;
    }

    // lista wspórzędnych, gdzie może pojawić się dziecko
    // wsp parzyste -> wspY
    // wsp nieparzyste -> wspX
    // dziecko pojawia się zawsze w pierwszej wolnej znalezionej przestrzeni.
    protected static List<Integer> getWspDziecka() {
        return wspDziecka;
    }

    public int getWspXSasiada() {
        return wspXSasiada;
    }

    public int getWspYSasiada() {
        return wspYSasiada;
    }

    public Polozenie getPolozenie() {
        return polozenie;
    }

    public void setPolozenie(Polozenie polozenie) {
        this.polozenie = polozenie;
    }
}
