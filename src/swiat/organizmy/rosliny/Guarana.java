package swiat.organizmy.rosliny;

import swiat.Swiat;
import swiat.organizmy.Rosliny;
import swiat.organizmy.Zwierzeta;
import swiat.polozenie.Polozenie;

public class Guarana extends Rosliny {
    public Guarana(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 0;
        this.sila = 0;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 641;
    }

    @Override
    public void kolizja() {
        if (swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Zwierzeta) {
            swiat.getOrganizm(wspYSasiada, wspXSasiada).setSila(swiat.getOrganizm(wspYSasiada, wspXSasiada).getSila() + 3);
            System.out.print(obraz + "(" + polozenie.getWspY()+","+polozenie.getWspX()+") rozprzestrzeniło się na: ("+wspYSasiada + "," + wspXSasiada + ").");
            System.out.println("Siła " + swiat.getOrganizm(wspYSasiada, wspXSasiada).getObraz() + "(" + wspYSasiada + "," + wspXSasiada + ")" + " wynosi teraz: " + swiat.getOrganizm(wspYSasiada, wspXSasiada).getSila());
        }
    }
}
