package swiat.organizmy.rosliny;

import swiat.Swiat;
import swiat.organizmy.Rosliny;
import swiat.organizmy.Zwierzeta;
import swiat.organizmy.zwierzeta.Czlowiek;
import swiat.polozenie.Polozenie;

public class WilczeJagody extends Rosliny {
    public WilczeJagody(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 0;
        this.sila = 99;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 631;
    }

    @Override
    public void kolizja() {
        if (swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Czlowiek) {
            if (((Czlowiek) swiat.getOrganizm(wspYSasiada, wspXSasiada)).isCzyUmiejetnoscSpecjalnaAktywna()) {
                switch (((Czlowiek) swiat.getOrganizm(wspYSasiada, wspXSasiada)).getNrAktywnejUmiejetnosci()) {
                    case 1:
                        System.out.print(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") chciało rozprzestrzenić się na: (" + wspYSasiada + "," + wspXSasiada + ").");
                        System.out.println(" Ale " + swiat.getOrganizm(wspYSasiada, wspXSasiada).getObraz() + "(" + wspYSasiada + "," + wspXSasiada + ")" + " jest nieśmiertelny.");

                }
            }
        } else if (swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Zwierzeta) {
            System.out.print(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") rozprzestrzeniło się na: (" + wspYSasiada + "," + wspXSasiada + ").");
            System.out.println(swiat.getOrganizm(wspYSasiada, wspXSasiada).getObraz() + "(" + wspYSasiada + "," + wspXSasiada + ")" + " zjadł: " + obraz + " i teraz nie żyje...");

            swiat.getOrganizm(wspYSasiada, wspXSasiada).setCzyZyje(false);
            swiat.setOrganizm(null, wspYSasiada, wspXSasiada);
            System.out.println("powstała nowa: " + obraz + " (" + wspYSasiada + "," + wspXSasiada + ")");
            swiat.dajMlodyOrganizm(wspYSasiada, wspXSasiada);
        }
    }
}
