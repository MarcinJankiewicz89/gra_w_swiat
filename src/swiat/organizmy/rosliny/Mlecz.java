package swiat.organizmy.rosliny;

import swiat.Swiat;
import swiat.organizmy.Rosliny;
import swiat.polozenie.Polozenie;

public class Mlecz extends Rosliny {
    public Mlecz(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 0;
        this.sila = 0;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 653;
    }

    @Override
    public void akcja(){
        for(int i = 0 ; i < 3 ; i++){
            super.akcja();
        }
    }

}
