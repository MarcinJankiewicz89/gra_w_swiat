package swiat.organizmy;


public abstract class Rosliny extends Organizm {

    @Override
    public void akcja() {
        int prawdZasiania = 10; //Prawdopodobieństwo, z jakim roślina się rozsieje.
        losujKierunekRuchu();
        if (losowaLiczba(100) < prawdZasiania) {
            if (sprawdzCzyPolePuste()) {
                ustawWSP_polaPustego();
                swiat.dajMlodyOrganizm(wspYPolePuste, wspXPolePuste);
                System.out.print("Z rośliny: " + swiat.zwrocOrganizm().obraz + " (" + swiat.zwrocOrganizm().polozenie.getWspY() + "," + swiat.zwrocOrganizm().polozenie.getWspX() + ") ");
                System.out.println("powstała nowa: " + swiat.getOrganizm(wspYPolePuste, wspXPolePuste).obraz + " (" + wspYPolePuste + "," + wspXPolePuste + ")");
            } else {
                ustawWSP_polaSasiada();
                kolizja();
            }
        }
    }

    @Override
    public void kolizja() {
    }


}
