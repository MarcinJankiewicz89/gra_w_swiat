package swiat.organizmy.zwierzeta;

import swiat.Swiat;
import swiat.organizmy.Zwierzeta;
import swiat.polozenie.Polozenie;

public class Lis extends Zwierzeta {
    public Lis(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 7;
        this.sila = 3;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 313;
    }

    @Override
    public void kolizja(){
        if(obraz==swiat.getOrganizm(wspYSasiada,wspXSasiada).getObraz()){
            super.kolizja();
        }
        //Lis unika ruchu jeśli jego sąsiad jest od niego silniejszy
        else if(sila<swiat.getOrganizm(wspYSasiada,wspXSasiada).getSila()){
            System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") i " + swiat.getOrganizm(wspYSasiada,wspXSasiada).getObraz() + "(" + wspYSasiada + "," + wspXSasiada + ") mieli walczyć");
            System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") nie ruszy się.");
        }
        else {
            super.kolizja();
        }
    }
}
