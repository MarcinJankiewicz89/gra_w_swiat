package swiat.organizmy.zwierzeta;

import swiat.Swiat;
import swiat.organizmy.Zwierzeta;
import swiat.polozenie.Polozenie;

public class Owca extends Zwierzeta {
    public Owca(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 4;
        this.sila = 4;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 466;
    }


}
