package swiat.organizmy.zwierzeta;

import swiat.Swiat;
import swiat.organizmy.Zwierzeta;
import swiat.polozenie.Polozenie;

public class Wilk extends Zwierzeta {
    public Wilk(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 5;
        this.sila = 9;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 372;
    }


}
