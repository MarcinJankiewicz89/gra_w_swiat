package swiat.organizmy.zwierzeta;

import swiat.Swiat;
import swiat.organizmy.Organizm;
import swiat.organizmy.Zwierzeta;
import swiat.polozenie.Polozenie;

import java.util.Scanner;

public class Czlowiek extends Zwierzeta {
    private char kierunekRuchuChar;
    private char specjalSkill;
    private boolean czyUmiejetnoscSpecjalnaAktywna = false;
    private int licznikTur;
    private int reset1, reset2, reset3, reset4, reset5;
    private int nrAktywnejUmiejetnosci;
    private Organizm chwilowyOrganizm;
    private Polozenie chwilowePolozenie;
    private int wspYtymczasowa;
    private int wspXtymczasowa;
    private boolean czyKolizyjnosc;
    private int silaStartowa;

    public Czlowiek(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 4;
        this.sila = 5;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 542;
    }

    @Override
    public void akcja() {
        System.out.println("Um specjal: 1 - Nieśmiertelność, 2 - Magiczny Eliksir, 3 - Szybkość Antylopy, 4 - Tarcza Alzura, 5 - Całopalenie");
        if (czyUmiejetnoscSpecjalnaAktywna) {
            switch (nrAktywnejUmiejetnosci) {
                case 2:
                    if (silaStartowa < sila) {
                        sila--;
                        System.out.println("Siła spadła do " + sila);
                    }
                    break;
                case 3:
                    if (licznikTur >= wiek) {
                        if (licznikTur - wiek >= 2) {
                            okreslKierunekRuchu();
                            if (sprawdzCzyPolePuste())
                                ruszSie();
                            else {
                                ustawWSP_polaSasiada();
                                kolizja();
                            }
                        } else if (losowaLiczba(100) < 50) {
                            okreslKierunekRuchu();
                            if (sprawdzCzyPolePuste())
                                ruszSie();
                            else {
                                ustawWSP_polaSasiada();
                                kolizja();
                            }
                        }
                    }
                    swiat.setWspYorganizmu(polozenie.getWspY());
                    swiat.setWspXorganizmu(polozenie.getWspX());
                    break;
                default:
            }
        }
        okreslKierunekRuchu();
        if (sprawdzCzyPolePuste())
            ruszSie();
        else {
            ustawWSP_polaSasiada();
            kolizja();
        }
    }

    public void akcjaLosowa() {
        losujKierunekRuchu();
        if (sprawdzCzyPolePuste()) {
            ruszSie();
            if (czyKolizyjnosc) {
                swiat.setOrganizm(chwilowyOrganizm, wspYtymczasowa, wspXtymczasowa);
                swiat.getOrganizm(wspYtymczasowa, wspXtymczasowa).getPolozenie().setWspY(wspYtymczasowa);
                swiat.getOrganizm(wspYtymczasowa, wspXtymczasowa).getPolozenie().setWspX(wspXtymczasowa);
                swiat.getOrganizm(wspYtymczasowa, wspXtymczasowa).setPolozenie(chwilowePolozenie);
            }
        } else {
            ustawWSP_polaSasiada();
            if (czyKolizyjnosc) {
                swiat.setOrganizm(chwilowyOrganizm, wspYtymczasowa, wspXtymczasowa);
                swiat.getOrganizm(wspYtymczasowa, wspXtymczasowa).getPolozenie().setWspY(wspYtymczasowa);
                swiat.getOrganizm(wspYtymczasowa, wspXtymczasowa).getPolozenie().setWspX(wspXtymczasowa);
                swiat.getOrganizm(wspYtymczasowa, wspXtymczasowa).setPolozenie(chwilowePolozenie);
            }
            czyKolizyjnosc = false;
            kolizja();
        }
    }

    public void ustawChwilowyOrganizm(int wspY, int wspX) {
        chwilowyOrganizm = swiat.getOrganizm(wspY, wspX);
        chwilowePolozenie = swiat.getOrganizm(wspY, wspX).getPolozenie();
        wspYtymczasowa = wspY;
        wspXtymczasowa = wspX;
        czyKolizyjnosc = true;
    }

    @Override
    public void kolizja() {
        if (czyUmiejetnoscSpecjalnaAktywna) {
            switch (nrAktywnejUmiejetnosci) {
                case 1:
                    if (sila >= swiat.getOrganizm(wspYSasiada, wspXSasiada).getSila()) {
                        super.kolizja();
                    } else {
                        System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") jest nieśmiertelny i unika walki z " + swiat.getOrganizm(wspYSasiada, wspXSasiada).getObraz() + "(" + wspYSasiada + "," + wspXSasiada + ") idzie dalej...");

                        chwilowyOrganizm = swiat.getOrganizm(wspYSasiada, wspXSasiada);
                        chwilowePolozenie = swiat.getOrganizm(wspYSasiada, wspXSasiada).getPolozenie();
                        wspYtymczasowa = wspYSasiada;
                        wspXtymczasowa = wspXSasiada;
                        czyKolizyjnosc = true;
                        this.ruszSie();
                        // akcja
                        akcjaLosowa();
                    }
                    break;
                case 4:
                    if (swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Zwierzeta) {
                        if (sila >= swiat.getOrganizm(wspYSasiada, wspXSasiada).getSila()) {
                            super.kolizja();
                        } else {
                            System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") ma Tarczę Alzura i odpycha " + swiat.getOrganizm(wspYSasiada, wspXSasiada).getObraz() + "(" + wspYSasiada + "," + wspXSasiada + ")");
                            // Tutaj powinna być akcja zwierzęcia
                            int wspYczlowieka = polozenie.getWspY();
                            int wspXczlowieka = polozenie.getWspX();
                            int kierunekCzlowieka = kierunekRuchu;
                            Polozenie polozenieCzlowieka = polozenie;
                            Organizm organizmCzlowieka = this;
                            swiat.setOrganizm(null, wspYczlowieka, wspXczlowieka);
                            swiat.setWspYorganizmu(wspYSasiada);
                            swiat.setWspXorganizmu(wspXSasiada);
                            swiat.zwrocOrganizm().setPolozenie(swiat.getOrganizm(wspYSasiada, wspXSasiada).getPolozenie());
                            swiat.zwrocOrganizm().getPolozenie().setWspY(wspYSasiada);
                            swiat.zwrocOrganizm().getPolozenie().setWspX(wspXSasiada);
                            swiat.getOrganizm(wspYSasiada, wspXSasiada).akcja();

                            swiat.setWspYorganizmu(wspYczlowieka);
                            swiat.setWspXorganizmu(wspXczlowieka);
                            swiat.setOrganizm(organizmCzlowieka, wspYczlowieka, wspXczlowieka);
                            swiat.zwrocOrganizm().setKierunekRuchu(kierunekCzlowieka);
                            swiat.zwrocOrganizm().setPolozenie(polozenieCzlowieka);
                            swiat.zwrocOrganizm().getPolozenie().setWspY(wspYczlowieka);
                            swiat.zwrocOrganizm().getPolozenie().setWspX(wspXczlowieka);
                            ruszSie();
                        }
                    } else
                        super.kolizja();
                    break;

                default:
                    super.kolizja();
            }
        } else {
            super.kolizja();
        }

    }

    private void okreslKierunekRuchu() {
        okreslMozliwosciRuchu();
        boolean czyWybranoKierunek = false;
        do {
            wybierzKierunekRuchuiSpecjal();
            switch (kierunekRuchuChar) {
                case 'a':
                    kierunekRuchu = 1;
                    if (ruchLewo)
                        czyWybranoKierunek = true;
                    else
                        System.out.println("Ruch " + obraz + " w lewo niemożliwy.");
                    break;
                case 'd':
                    kierunekRuchu = 2;
                    if (ruchPrawo)
                        czyWybranoKierunek = true;
                    else
                        System.out.println("Ruch " + obraz + " w prawo niemożliwy.");
                    break;
                case 'w':
                    kierunekRuchu = 3;
                    if (ruchGora)
                        czyWybranoKierunek = true;
                    else
                        System.out.println("Ruch " + obraz + " w górę niemożliwy.");
                    break;
                case 's':
                    kierunekRuchu = 4;
                    if (ruchDol)
                        czyWybranoKierunek = true;
                    else
                        System.out.println("Ruch " + obraz + " w dół niemożliwy.");
                    break;
                default:
                    System.out.println("Wybierz poprawny/możliwy kierunek ruchu");
            }
        } while (!czyWybranoKierunek);
    }

    private void wybierzKierunekRuchuiSpecjal() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj kierunek ruchu 'w', 'a', 's', 'd'. I umiejętność specjalną do aktywowania 1-5. Np. w3");
        String polecenie = scanner.next();
        if (wiek == licznikTur) {
            czyUmiejetnoscSpecjalnaAktywna = false;
        }
        if (polecenie.length() == 1) {
            kierunekRuchuChar = polecenie.charAt(0);
        } else {
            kierunekRuchuChar = polecenie.charAt(0);
            if (!czyUmiejetnoscSpecjalnaAktywna) {
                specjalSkill = polecenie.charAt(1);
                wybierzUmiejetnoscSpecjalna();
            } else {
                System.out.println("Obecna umiejętność specjalna będzie trwać jeszcze: " + (licznikTur - wiek) + " tur.");
            }
        }
    }

    private void wybierzUmiejetnoscSpecjalna() {
        switch (specjalSkill) {

            case '1': // Nieśmiertelność
                if (reset1 <= wiek) {
                    System.out.println("Nieśmiertelność aktywna");
                    czyUmiejetnoscSpecjalnaAktywna = true;
                    nrAktywnejUmiejetnosci = 1;
                    reset1 = wiek + 10;
                } else {
                    System.out.println("Nieśmiertelność można aktywować za " + (reset1 - wiek) + " rund.");
                }

                break;
            case '2': // Magiczny Eliksir
                if (reset2 <= wiek) {
                    if (sila < 10) {
                        silaStartowa = sila;
                        sila = 10;
                        System.out.println("Magiczny Eliksir wypity, siła " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") wynosi teraz: " + sila);
                        czyUmiejetnoscSpecjalnaAktywna = true;
                        nrAktywnejUmiejetnosci = 2;
                        reset2 = wiek + 10;
                    } else
                        System.out.println("Magiczny Eliksir nie wpłynie, na siłę " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ")");
                } else {
                    System.out.println("Magiczny Eliksir wypić za " + (reset2 - wiek) + " rund.");
                }
                break;
            case '3': //Szybkość Antylopy
                if (reset3 <= wiek) {
                    System.out.println("Szybkość Antylopy aktywna");
                    czyUmiejetnoscSpecjalnaAktywna = true;
                    nrAktywnejUmiejetnosci = 3;
                    reset3 = wiek + 10;
                } else {
                    System.out.println("Szybkość Antylopy można aktywować za " + (reset3 - wiek) + " rund.");
                }
                break;
            case '4': //Tarcza Alzura
                if (reset4 <= wiek) {
                    System.out.println("Tarcza Alzura aktywna");
                    czyUmiejetnoscSpecjalnaAktywna = true;
                    nrAktywnejUmiejetnosci = 4;
                    reset4 = wiek + 10;
                } else {
                    System.out.println("Tarczę Alzura można aktywować za " + (reset4 - wiek) + " rund.");
                }
                break;
            case '5': //Całopalenie
                if (reset5 <= wiek) {
                    System.out.println("Całopalenie");
                    czyUmiejetnoscSpecjalnaAktywna = true;
                    nrAktywnejUmiejetnosci = 5;
                    reset5 = wiek + 10;
                    spalSasiadow();
                } else {
                    System.out.println("Całopalenie można aktywować za " + (reset5 - wiek) + " rund.");
                }
                break;
            default:
                System.out.println("Nie rozpoznano wyboru um. specjalnej");
                nrAktywnejUmiejetnosci = 0;
        }
        licznikTur = wiek + 5;
    }

    private void spalSasiadow() {
        if (ruchLewo)
            if (swiat.getOrganizmy()[polozenie.getWspY()][polozenie.getWspX() - krok] != null) {
                // Palimy sąsiada z lewej
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY()][polozenie.getWspX() - krok].getObraz() + "(" + polozenie.getWspY() + "," + (polozenie.getWspX() - krok) + ")");
                swiat.getOrganizm(polozenie.getWspY(), polozenie.getWspX() - krok).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY(), polozenie.getWspX() - krok);
            }
        if (ruchPrawo)
            if (swiat.getOrganizmy()[polozenie.getWspY()][polozenie.getWspX() + krok] != null) {
                // Palimy sąsiada z prawej
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY()][polozenie.getWspX() + krok].getObraz() + "(" + polozenie.getWspY() + "," + (polozenie.getWspX() + krok) + ")");
                swiat.getOrganizm(polozenie.getWspY(), polozenie.getWspX() + krok).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY(), polozenie.getWspX() + krok);
            }
        if (ruchGora)
            if (swiat.getOrganizmy()[polozenie.getWspY() - krok][polozenie.getWspX()] != null) {
                // Palimy sąsiada z gory
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY() - krok][polozenie.getWspX()].getObraz() + "(" + (polozenie.getWspY() - krok) + "," + polozenie.getWspX() + ")");
                swiat.getOrganizm(polozenie.getWspY() - krok, polozenie.getWspX()).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY() - krok, polozenie.getWspX());
            }
        if (ruchDol)
            if (swiat.getOrganizmy()[polozenie.getWspY() + krok][polozenie.getWspX()] != null) {
                // Palimy sąsiada z dołu
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY() + krok][polozenie.getWspX()].getObraz() + "(" + (polozenie.getWspY() + krok) + "," + polozenie.getWspX() + ")");
                swiat.getOrganizm(polozenie.getWspY() + krok, polozenie.getWspX()).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY() + krok, polozenie.getWspX());
            }

        if (ruchLewo && ruchGora)
            if (swiat.getOrganizmy()[polozenie.getWspY() - krok][polozenie.getWspX() - krok] != null) {
                // Palimy sąsiada po skosie z lewej na gorze
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY() - krok][polozenie.getWspX() - krok].getObraz() + "(" + (polozenie.getWspY() - krok) + "," + (polozenie.getWspX() - krok) + ")");
                swiat.getOrganizm(polozenie.getWspY() - krok, polozenie.getWspX() - krok).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY() - krok, polozenie.getWspX() - krok);
            }

        if (ruchLewo && ruchDol)
            if (swiat.getOrganizmy()[polozenie.getWspY() + krok][polozenie.getWspX() - krok] != null) {
                // Palimy sąsiada po skosie z lewej na dole
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY() + krok][polozenie.getWspX() - krok].getObraz() + "(" + (polozenie.getWspY() + krok) + "," + (polozenie.getWspX() - krok) + ")");
                swiat.getOrganizm(polozenie.getWspY() + krok, polozenie.getWspX() - krok).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY() + krok, polozenie.getWspX() - krok);
            }

        if (ruchPrawo && ruchGora)
            if (swiat.getOrganizmy()[polozenie.getWspY() - krok][polozenie.getWspX() + krok] != null) {
                // Palimy sąsiada po skosie z prawej na gorze
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY() - krok][polozenie.getWspX() + krok].getObraz() + "(" + (polozenie.getWspY() - krok) + "," + (polozenie.getWspX() + krok) + ")");
                swiat.getOrganizm(polozenie.getWspY() - krok, polozenie.getWspX() + krok).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY() - krok, polozenie.getWspX() + krok);
            }
        if (ruchPrawo && ruchDol)
            if (swiat.getOrganizmy()[polozenie.getWspY() + krok][polozenie.getWspX() + krok] != null) {
                // Palimy sąsiada po skosie z prawej na dole
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") spalił " + swiat.getOrganizmy()[polozenie.getWspY() + krok][polozenie.getWspX() + krok].getObraz() + "(" + (polozenie.getWspY() + krok) + "," + (polozenie.getWspX() + krok) + ")");
                swiat.getOrganizm(polozenie.getWspY() + krok, polozenie.getWspX() + krok).setCzyZyje(false);
                swiat.setOrganizm(null, polozenie.getWspY() + krok, polozenie.getWspX() + krok);
            }
    }

    public void setCzyKolizyjnosc(boolean czyKolizyjnosc) {
        this.czyKolizyjnosc = czyKolizyjnosc;
    }

    public boolean isCzyUmiejetnoscSpecjalnaAktywna() {
        return czyUmiejetnoscSpecjalnaAktywna;
    }

    public int getNrAktywnejUmiejetnosci() {
        return nrAktywnejUmiejetnosci;
    }

    public boolean isCzyKolizyjnosc() {
        return czyKolizyjnosc;
    }

    public Organizm getChwilowyOrganizm() {
        return chwilowyOrganizm;
    }

    public Polozenie getChwilowePolozenie() {
        return chwilowePolozenie;
    }

    public int getWspYtymczasowa() {
        return wspYtymczasowa;
    }

    public int getWspXtymczasowa() {
        return wspXtymczasowa;
    }


}
