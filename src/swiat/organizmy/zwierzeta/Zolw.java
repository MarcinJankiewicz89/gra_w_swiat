package swiat.organizmy.zwierzeta;

import swiat.Swiat;
import swiat.organizmy.Zwierzeta;
import swiat.polozenie.Polozenie;

import java.util.Random;

public class Zolw extends Zwierzeta {
    public Zolw(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 1;
        this.sila = 2;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 381;
    }

    @Override
    public void akcja() {
        if(losowaLiczba(100)<25){
            super.akcja();
        }
        else
            System.out.println(obraz+ "(" + polozenie.getWspY()+","+polozenie.getWspX()+") nie ruszył się.");
    }

}
