package swiat.organizmy.zwierzeta;

import swiat.Swiat;
import swiat.organizmy.Organizm;
import swiat.organizmy.Zwierzeta;
import swiat.polozenie.Polozenie;


public class Antylopa extends Zwierzeta {
    public Antylopa(Swiat swiatWKtorymIstnieje, int wspY, int wspX) {
        this.swiat = swiatWKtorymIstnieje;
        this.inicjatywa = 4;
        this.sila = 4;
        this.krok = 1;
        this.wiek = 0;
        this.czyZyje = true;
        this.polozenie = new Polozenie(wspY, wspX);
        this.obraz = 258;
    }

    // Muszę dopracować możliwość ruchu w 2. turze. Gdy Antylopa się przesuwa w kolejnej turze zwracany Organizm jest z pola pustego...
    @Override
    public void akcja() {
        for (int i = 0; i < 2; i++) { // powinno być i < 2
            if (czyZyje) {
                super.akcja();
                swiat.setWspYorganizmu(polozenie.getWspY());
                swiat.setWspXorganizmu(polozenie.getWspX());
            }
        }
    }

    @Override
    public void kolizja() {
        if (obraz == swiat.getOrganizm(wspYSasiada, wspXSasiada).getObraz()) {
            super.kolizja();
        } else if (losowaLiczba(100) < 50) {
            // Antylopa unika walki, przesuwa się na niezajęte pole
            System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") unika walki z " + swiat.getOrganizm(wspYSasiada, wspXSasiada).getObraz() + "(" + wspYSasiada + "," + wspXSasiada + ") ");
            Polozenie pustePolozenie = zwrocPustePolozenie();
            if (pustePolozenie.porownajPolozenia(polozenie.getWspY(), polozenie.getWspX())) {
                System.out.println("i " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") wraca na swoją pozycję na: (" + polozenie.getWspY() + "," + polozenie.getWspX() + ")");
            } else {
                System.out.print("i " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") przeskakuje na: (");
                Organizm chwilowyOrganizm = swiat.getOrganizm(polozenie.getWspY(), polozenie.getWspX());
                swiat.setOrganizm(null);
                this.polozenie = pustePolozenie;
                swiat.setOrganizm(chwilowyOrganizm, polozenie.getWspY(), polozenie.getWspX());
                System.out.println(polozenie.getWspY() + "," + polozenie.getWspX() + ")");
            }
        } else {
            super.kolizja();
        }
    }


}
