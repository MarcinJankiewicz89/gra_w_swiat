package swiat.organizmy;

import swiat.organizmy.rosliny.Guarana;
import swiat.organizmy.zwierzeta.Antylopa;
import swiat.organizmy.zwierzeta.Czlowiek;
import swiat.organizmy.zwierzeta.Zolw;


public abstract class Zwierzeta extends Organizm {


    @Override
    public void akcja() {
        losujKierunekRuchu();
        if (sprawdzCzyPolePuste())
            ruszSie();
        else {
            ustawWSP_polaSasiada();
            kolizja();
        }
    }

    @Override
    public void kolizja() {
        char obrazSasiada = swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz;
        boolean czyDojdzieDoAtaku = true;
        if (this.obraz == obrazSasiada) {
            if (losowaLiczba(100) < 50) {
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") i " + obrazSasiada + "(" + wspYSasiada + "," + wspXSasiada + ") będą się rozmnażać");
                znajdzPustePola(wspYSasiada, wspXSasiada);
                if (getWspDziecka().size() != 0) {
                    swiat.dajMlodyOrganizm(getWspDziecka().get(0), getWspDziecka().get(1));
                    System.out.println("Rodzic 1: " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") i 2: " + obrazSasiada + "(" + wspYSasiada + "," + wspXSasiada + ") dają dziecko na pozycji: " + swiat.getOrganizm(getWspDziecka().get(0), getWspDziecka().get(1)).obraz + "(" + getWspDziecka().get(0) + "," + getWspDziecka().get(1) + ")");
                } else {
                    System.out.println("Brak miejsca na potomstwo");
                }
            }
            else{
                System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") i " + obrazSasiada + "(" + wspYSasiada + "," + wspXSasiada + ") tym razem nie dali dziecka");
            }

        } else { // Tu kończy się rozmnażanie i zaczyna walka:
            System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") i " + obrazSasiada + "(" + wspYSasiada + "," + wspXSasiada + ") będą walczyć");
            // Co się dzieje, gdy zwierze zje Guaranę:
            if (swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Guarana) {
                this.setSila(this.getSila() + 3);
                System.out.println("Siła " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") wynosi teraz: " + this.getSila());
            }
            if ((swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Zolw) && (this.getSila() < 5)) {
                czyDojdzieDoAtaku = false;
                System.out.print(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") zaatakował: " + swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ") ale ");
                System.out.println(swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ") odepchnął na swoją poprzednią pozycję: " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ")");
            }
            if ((swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Antylopa) && losowaLiczba(100) < 50) {
                Organizm chwilowyOrganizm = swiat.getOrganizm(wspYSasiada, wspXSasiada);
                chwilowyOrganizm.polozenie = zwrocPustePolozenie();
                if (chwilowyOrganizm.polozenie.porownajPolozenia(polozenie.getWspY(), polozenie.getWspX())) {
                    System.out.println(obrazSasiada + "(" + wspYSasiada + "," + wspXSasiada + ") nie ma gdzie uciec w walce z: " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") ");
                } else {
                    System.out.println(obrazSasiada + "(" + wspYSasiada + "," + wspXSasiada + ") unika walki z: " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") ");
                    System.out.print("i " + obrazSasiada + "(" + wspYSasiada + "," + wspXSasiada + ") przeskakuje na: (");
                    swiat.setOrganizm(null, wspYSasiada, wspXSasiada);
                    swiat.setOrganizm(chwilowyOrganizm, chwilowyOrganizm.polozenie.getWspY(), chwilowyOrganizm.polozenie.getWspX());
                    System.out.println(chwilowyOrganizm.polozenie.getWspY() + "," + chwilowyOrganizm.polozenie.getWspX() + ")");
                    czyDojdzieDoAtaku = false;
                    this.ruszSie();
                }
            }


            if (czyDojdzieDoAtaku) {
                // Następnie dochodzi do walki, wygrywa silniejszy organizm (lub ten, który zaatakował)
                if (this.sila >= swiat.getOrganizm(wspYSasiada, wspXSasiada).sila) {
                    if (swiat.getOrganizm(wspYSasiada, wspXSasiada) instanceof Czlowiek) {
                        if (((Czlowiek) swiat.getOrganizm(wspYSasiada, wspXSasiada)).isCzyUmiejetnoscSpecjalnaAktywna()) {
                            switch (((Czlowiek) swiat.getOrganizm(wspYSasiada, wspXSasiada)).getNrAktywnejUmiejetnosci()) {
                                case 1: // Powinno być, że czlowiek idzie na inne pole, ale zrobię, że atakujący wraca na swoje pole
                                    // Człowiek odpycha jak zółw
                                    System.out.print(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") zaatakował: " + swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ") ale ");
                                    System.out.println(swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ") jest nieśmiertelny i odepchnął na swoją poprzednią pozycję: " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ")");
                                    break;
                                case 4: //Tutaj też zrobię odpychanie na poprzednie pole zamiast przejścia na
                                    System.out.print(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") zaatakował: " + swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ") ale ");
                                    System.out.println(swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ") ma Tarczę Alzura i odepchął go na poprzednie pole: " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ")");

                                    /*int wspOrgY = polozenie.getWspY();
                                    int wspOrgX = polozenie.getWspX();
                                    int wspYczlowieka = wspYSasiada;
                                    int wspXczlowieka = wspXSasiada;
                                    Polozenie polozenieCzlowieka = swiat.getOrganizm(wspYSasiada, wspXSasiada).getPolozenie();
                                    Organizm organizmCzlowieka = swiat.getOrganizm(wspYSasiada, wspXSasiada);

                                    swiat.setOrganizm(null, wspOrgY, wspOrgX);
                                    //
                                    swiat.setWspYorganizmu(wspYSasiada);
                                    swiat.setWspXorganizmu(wspXSasiada);
                                    polozenie = swiat.getOrganizm(wspYSasiada, wspXSasiada).getPolozenie();
                                    polozenie.setWspY(wspYSasiada);
                                    polozenie.setWspX(wspXSasiada);
                                    //
                                    this.akcja();

                                    swiat.setOrganizm(organizmCzlowieka, wspYczlowieka, wspXczlowieka);
                                    swiat.getOrganizm(wspYczlowieka, wspXczlowieka).setPolozenie(polozenieCzlowieka);
                                    swiat.getOrganizm(wspYczlowieka, wspXczlowieka).getPolozenie().setWspY(wspYczlowieka);
                                    swiat.getOrganizm(wspYczlowieka, wspXczlowieka).getPolozenie().setWspX(wspXczlowieka);
*/
                                    break;
                                default: {
                                    System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") pokonał: " + swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ")");
                                    swiat.getOrganizm(wspYSasiada, wspXSasiada).setCzyZyje(false);
                                    this.ruszSie();
                                }
                            }

                        } else {
                            System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") pokonał: " + swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ")");
                            swiat.getOrganizm(wspYSasiada, wspXSasiada).setCzyZyje(false);
                            this.ruszSie();
                        }
                    } else {
                        System.out.println(obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ") pokonał: " + swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ")");
                        swiat.getOrganizm(wspYSasiada, wspXSasiada).setCzyZyje(false);
                        this.ruszSie();
                    }
                } else {
                    System.out.println(swiat.getOrganizm(wspYSasiada, wspXSasiada).obraz + "(" + wspYSasiada + "," + wspXSasiada + ") pokonał: " + obraz + "(" + polozenie.getWspY() + "," + polozenie.getWspX() + ")");
                    this.setCzyZyje(false);
                    swiat.setOrganizm(null, polozenie.getWspY(), polozenie.getWspX());
                }
            }
        }
    }


}
