package swiat;

import swiat.organizmy.Organizm;
import swiat.organizmy.zwierzeta.Antylopa;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Swiat mojMalySwiat = new Swiat();
        mojMalySwiat.rozpocznijRozgrywke();
        mojMalySwiat.rysujSwiat();
        String polecenie;
        do {
            System.out.println("Co chesz zrobić? 't' - wykona turę, 'q' - zakończonie");
            polecenie = scanner.next();
            switch (polecenie) {
                case "t":
                    mojMalySwiat.wykonajTure();
                    System.out.println("______________________");
                    System.out.println("______________________");
                    mojMalySwiat.rysujSwiat();
                    break;
                case "q":
                    System.out.println("Do widzenia... ;)");
                    break;
                default:
                    System.out.println("Polecenie nieznane.");
            }

        } while (!polecenie.equals("q"));
    }
}
