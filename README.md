# Gra w Świat
Jest to projekt stworzony w języku Java jako jedno z zadań domowych w ramach kursu SDAcademy.


## Informacje ogólne
Celem projektu jest implementacja programu o charakterze symulatora wirtualnego świata, który ma mieć strukturę dwuwymiarowej kraty o dowolnym, zadanym przez użytkownika rozmiarze NxM. W świecie tym będą istniały proste formy życia o odmiennym zachowaniu. Każdy z organizmów zajmuje dokładnie jedno pole w tablicy, na każdym polu może znajdować się co najwyżej jeden organizm (w przypadku kolizji jeden z nich powinien zostać usunięty lub przesunięty). 

Symulator ma mieć charakter turowy. W każdej turze wszystkie organizmy istniejące na świecie mają wykonać akcję stosowną do ich rodzaju. Część z nich będzie się poruszała (organizmy zwierzęce), część będzie nieruchoma (organizmy roślinne). W przypadku kolizji (jeden z organizmów znajdzie się na tym samym polu, co inny) jeden z organizmów zwycięża, zabijając (np. wilk) lub odganiając (np. żółw) konkurenta.
Kolejność ruchów organizmów w turze zależy od ich inicjatywy. Pierwsze ruszają się zwierzęta posiadające najwyższą inicjatywę. W przypadku zwierząt o takiej samej inicjatywie o kolejności decyduje zasada starszeństwa (pierwszy rusza się dłużej żyjący). Zwycięstwo przy spotkaniu zależy od siły organizmu. Przy równej sile zwycięża organizm, który zaatakował.  


## Człowiek - gracz
Specyficznym rodzajem zwierzęcia jest Człowiek. W przeciwieństwie do zwierząt, człowiek nie porusza się w sposób losowy. Kierunek jego ruchu jest determinowany przed rozpoczęciem tury za pomocą odpowiednich liter na klawiaturze. Człowiek posiada też specjalną umiejętność, którą można aktywować osobnym przyciskiem. Aktywowana umiejętność pozostaje czynna przez 5 kolejnych tur, po czym następuje jej dezaktywacja. Po dezaktywacji umiejętność nie może być aktywowana przed upływem 5 kolejnych tur.

## Podstawowe cechy organizmów
| Cecha | opis |
| ------ | ------ |
| Siła | Silniejszy organizm wygrywa walkę (są wyjątki) |
| Inicjatywa| organizm z większą inicjatywą pierwszy wykonuje swoją turę, w drugiej kolejności decyduje wiek (starszy ma pierwszeństwo) |
| Symbol | symbol reprezentujący dany organizm na mapie |


## Poruszanie się
| przycisk | opis |
| ------ | ------ |
| w | ruch do góry |
| s | ruch do dołu |
| a | ruch w lewo |
| d | ruch w prawo |
| 1-5 | użycie specjalnej umiejętności |


## Specjalne umiejętności człowieka
| id | Umiejętność | Cechy |
| ------ | ------ | ------|
| 1 | Nieśmiertelność | Człowiek nie może zostać zabity. W przypadku konfrontacji z silniejszym przeciwnikiem przesuwa się na losowo wybrane pole sąsiadujące.
| 2 | Magiczny Eliksir | Siła Człowieka rośnie do 10 w pierwszej turze działania umiejętności. W każdej kolejej turze maleje o "1", aż wróci do stanu początkowego.
| 3 | Szybkość Antylopy | Człowiek porusza się na odległość dwóch pól zamiast jednego przez pierwsze 3 tury działania umiejętności. W pozostałych 2 turach szansa, że umiejętność zadziała wynosi 50%.
| 4 | Tarcza Alzura | Człowiek odstrasza wszystkie zwierzęta. Zwierzę, które stanie na polu Człowieka zostaje przesunięte na losowe pole sąsiednie.
| 5 | Całopalanie | Człowiek niszczy wszystkie rośliny i zwierzęta znajdujące się na polach sąsiadujących z jego pozycją.


## Zwierzęta
| symbol | nazwa | siła | inicjatywa | specyfika metody akcja() | specyfika metody kolizja() |
| ------ | ------ | ------| ---- | ------|------|
| Ȟ | Człowiek | 5 | 4 | może używać mocy | w zależności od umiejętności specjalnej |
| Ŵ | Wilk | 9 | 5 | - | - |
| Ă | Antylopa | 4 | 4 | Zasięg ruchu wynosi 2 pola | 50% szans na ucieczkę przed walką. Wówczas przesuwa się na niezajęte sąsiednie pole |
| ǒ | Owca| 4 | 4 | - | - |
| Ĺ | Lis | 3 | 7 | Dobry węch: nigdy nie rusza się na pole zajmowane przez organizm silniejszy od niego | - |
| Ž | Żółw | 2 | 1 | 75% szans, że nie zmieni swojego położenia | Odpiera ataki zwierząt o sile < 5. Napastnik wraca na swoje pole |


## Rośliny
| symbol | nazwa | siła | inicjatywa | specyfika metody akcja() | specyfika metody kolizja() |
| ------ | ------ | ------| ---- | ------|------|
| ɤ |   Trawa | 0 | 0 |  - | - |
| ʍ |   Mlecz | 0 | 0 |  Podejmuje 3 próby rozprzestrzeniania w jednej turze | - |
| ʁ |   Guarana | 0 | 0 |  - | Zwiększa siłę zwierzęcia, które zjadło tę roślinę o 3 |
| ɷ |   Wilcze Jagody | 99 | 0 |  - | Zwierzę, które zjadło tę roślinę ginie |